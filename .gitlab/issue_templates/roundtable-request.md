# Diversity, Inclusion, & Belonging Facilitation, Manager Request 

This issue asks the DIB Team to provide facilitation services to your team. 

**Manager To do:**

[ ] Tag DIB Team: @gitlab-com/people-group/dib-diversity-inclusion-and-belonging 
[ ] Complete training questions 

**DIB Team To Do:**

[ ] Set up time sync or async with the manager
[ ] Confirm Content
[ ] Lock in a date and time for session 
[ ] Prepare any prework and agenda 

### Manager Training Questions:

**Training Content**

[ ] DIB Roundtable 
[ ] Allyship Training
[ ] Unconscious Bias 
[ ] Culturally training 
[ ] Inclusive Language Training 
[ ] Other, please specify 

**Team Information:** 

- Team Size:
- Team locations: 
- Department: 
- Reason for training: 
- Ideal dates and times: 

### Any other information:
