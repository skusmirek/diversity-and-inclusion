# Diversity, Inclusion & Belonging Roundtables 

### Information to Review

Review the [DIB Roundtable Page](https://about.gitlab.com/company/culture/inclusion/dib-roundtables/) ensure that you are familiar with the format and ground rules. 

### Roundtable Members

1. ADD TEAM MEMBER
2. ADD TEAM MEMBER
3. ADD TEAM MEMBER
4. ADD TEAM MEMBER
5. ADD TEAM MEMBER
6. ADD TEAM MEMBER

### Roundtable Information

- Date: 
- Time: 
- Facilitator: 

/label ~"DIB Roundtable"
