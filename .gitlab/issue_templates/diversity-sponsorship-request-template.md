# Diversity and Inclusion Event Sponsorship Request

<!--
Note: If you are applying this template to an existing issue, please follow these steps:
1. Copy text from existing issue.
2. Apply 'sponsorship-request' template to issue.
3. Paste text from original issue into the appropriate field at the bottom of this template.
4. Update remaining fields.
-->

### Steps for Team Member requesting sponsorship

- [ ] Confirm the issue is confidential
- [ ] Confirm the issue is assigned to @lmcnally1
- [ ] Confirm the issue has the _DIB Sponsorship_ label

## Event Info

Please include the following information about your event:

- Event/Group Name:
- Event/Group URL:
- Organizer Name:
- Location:
- Date:
- Time:
- Will there be a speaker from the wider GitLab community or a talk about GitLab?
- Number of attendees:
- Audience profile (ex: developers, executives, students, non-technical, etc.):
- Prospectus:

## Diversity, Inclusion & Belonging Requirements:

For the event to be considered for sponsorship it must have a Diversity, Inclusion and Belonging element. It is not enough for the event to include underrepresented groups as this should be standard for all events. 

<!-- Use this space to address the requirements set out [here](https://about.gitlab.com/community/sponsorship/). Please be detailed as possible, touch on speaker demographics, audience demographics, subject matter etc to ensure we get a full overview of the Diversity, Inclusion & Belonging Nature of the event. -->


## Event Description

<!-- Use this space to describe your event and include anything important that we forgot to ask. -->

## Meeting notes: (DIB Team Member to Complete, not all requests will move to a meeting)

## Type of request

Check all that apply

- [ ] Speaker
- [ ] Sponsorship
- [ ] Swag

## Event Scorecard

For detail on how the scorecard is used, please see [How we assess events](https://about.gitlab.com/handbook/marketing/community-relations/evangelist-program/#how-we-assess-requests) in the GitLab Handbook.

| Criteria / Score   | 0 | 1  | 2 |
|----------------|---------------|---------------|----------------|
| GitLab content | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> |
| Audience type | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> |
| Attendee interaction | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> |
| Event location | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> |
| Meets Diversity, Inclusion & Belonging Requirements | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> |
| Diversity Opportunity | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> |
| Diversity Audience Size | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> |


## Business FYI's - DIB Team to tag after due diligence 
- [ ] Vice President, DIB (@sheridam)



/label ~DIB Sponsorship 
/assign @lmcnally1
/confidential
