# Diversity, Inclusion and Belonging, Quality Check for Learning & Development and External Training

## Before starting the QA Check:

- Have you been a part of developing the training or choosing the training provider? You'll have important context to add to the DIB and L&D review
- Have you completed the [Diversity, Inclusion, and Belonging Certification](https://about.gitlab.com/company/culture/inclusion/dib-training/) and bias training to sufficiently be able to spot DIB & bias issues? Taking this training will help you proactivly spot DIB and bias issues that may be present in the training.

## DIB QA Check process:

- [ ] Does any visual content express a visual representation of Diversity? 
- [ ] Does the content show any implicit or explicit bias with regards to gender, race, ethnicity, sexual orientation, diverse-ability, age or any other dimension? 
- [ ] Have all written content or transcripts gone through the inclusive language checker? (To be completed by the creator or Curator)(""Tag the creator/Curator here"")
- [ ] Does the training include reasonable pathways for diverseability team members? (for example close caption options) 
- [ ] Are there any other red or yellow flags in the content written or visual? (If so comment in the issue)
- [ ] Does video content include closed captioning, subtitles, or a text-based component?

## L&D QA process

- [ ] Are there clear learner objectives
- [ ] Is material handbook first
- [ ] Are multiple modalities of learning used: video, text, auido, visual, live, etc.
- [ ] Will the training be added to GitLab Learn
- [ ] Review for misspelling and grammar issues
- [ ] Time needed for user to complete the course is documented and added to GitLab Learn
- [ ] Do videos include closed captions? Public videos should use YouTube. Private videos need transcripted captions on the video.

## Does the Learning & Development Program or External Training pass?

- [ ] Yes 
- [ ] No 

## Not passed the DIB QA Check:

**In the comments of this issue be specific as possible:**

- Highlight the time in the video and which video
- Highlight the paragraph and the specific language
- Creator of the content, tag the reviewer once changes have been completed 


