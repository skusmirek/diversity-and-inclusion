## TMRG Lead Nomination issue 

<!--
Note: If you are applying this template to an existing issue, please follow these steps:
1. Name this issue: *name of TMRG** nomination: *your name*
2. Assign issue to @lmcnally1
-->

#### Selection Criteria (To be filled out by the DIB Team)

- [ ] The team member must be performing in their role and not part of any performance management process as confirmed by the People Group. 
- [ ] Must be a full-time GitLab team member. This includes PEO and full-time contractor team members. 
- [ ] At least 6-month tenure at GitLab
- [ ] Ideally already an active TMRG member if the TMRG already exists
- [ ] Approval from your direct manager, support to commit the time and to use this leadership role as a professional development opportunity
- [ ] A minimum one-year commitment in the role but understanding this may change to less and can be more.

#### Team Member Details:

**Name:** 
**Job Title:**
**Department:** 
**Manager:** 'Please tag your manager'

#### Why are you nominating yourself for TMRG Leadership? 

<!-- Use this space to address the requirements set out in the selection criteria and the qualities of a TMRG Lead. Use this to explain programs you would like to bring in as a TMRG Lead and the reasons why you are hoping to take on this position -->

#### What is your current TMRG Participation? 

<!-- Use this space to talk through TMRG activities you have participated or led in the past. Talk through your engagement with the TMRG and how long you have been a member for -->

#### Nomination decision (DIB Team)

- [ ] Successful 
- [ ] Not Successful 
