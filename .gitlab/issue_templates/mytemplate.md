## :bulb: Opportunity
* Proposed event title: 
* Speaker(s) Name:
* Speaker(s) Pronouns:
* Event or speaker summary:
* Sponsored or tied to TMRG?  
    * Please include details
*Relevant Links: 
* **This event/speaker is scheduled for [insert date].**

## :notebook: Project Details
* [ ] Recording event? yes/no
    * [ ] Speaker is aware event is being recorded
* [ ] GitLab internal host (will be listed as alternate host in zoom):
* [ ] Agenda document (will be included in calendar invite):

## :microphone2: Speaking Engagement Owners
* [ ] (`@____ `) to confirm Speaker
* [ ] (`@____ `) to send GitLab Team Meetings event 
* [ ] (`@____ `) to send create agenda
* [ ] (`@____ `) to submit any payment/remittance details in Coupa
* [ ] (`@____ `) update DIB internal handbook page with speaking engagement details

## :dart: Deadlines 
* Deadline for speaker/event needs to be confirmed: 

## :mega: Spread the Word!
* [ ] Slack: (`@____ `) to advertise in Slack 
    * List relevant slack channels to advertise:


cc: @sheridam @skusmirek @lmcnally1 @mbrgg

------

