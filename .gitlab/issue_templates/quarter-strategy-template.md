## [Insert TMRG Name] TMRG Quarterly Strategy 

- Which quarter is this for:
- TMRG Lead:  


### Vision
<!--
Note: Enter the vision for this quarter; what is goal, mission, purpose of the quarter. Ensure that your actions and measruables align with your vision. 
-->



### Actions

- Action & Plan (Copy and paste for multiple actions):
    - 
    - 
    - 
    - DRI: 

### Budget 

Please fill in the [TMRG Forecast Tracking Spreadsheet](https://docs.google.com/spreadsheets/d/1-BZrH1KsRBYl9r0EYEy6VXlKyvGGf585mwoj9Vz5rgA/edit#gid=1333381534) 


### Measurables 
<!--
Note: Enter the OKRs, KPIs, or Metrics you are hoping to achieve with your actions. This could simply be to put on two events or could be to help the recruiting team hire X number of URGs. Ensure that the measureables make sense with the actions. 
-->
