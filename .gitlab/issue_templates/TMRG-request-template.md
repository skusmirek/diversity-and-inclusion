# TMRG Request

<!--
Note: If you are applying this template to an existing issue, please follow these steps:
1. Copy text from existing issue.
2. Apply 'TMRG-request' template to issue.
3. Paste text from original issue into the appropriate field at the bottom of this template.
4. Update remaining fields.
-->

## TMRG Info

Please include the following information on the TMRG request:

- Suggested TMRG title 
- Purpose of the TMRG
- TMRG Diversity Dimension 
- Benefit of having this TMRG at GitLab 
- Suggested mission statement of the TMRG
- TMRG Description

## Suggested TRMG title

<!-- Use this space to describe your event and include anything important that we forgot to ask. -->

## Purpose of the TMRG

<!-- Use this space to describe your event and include anything important that we forgot to ask. -->

## TMRG Diversity Dimension

<!-- Use this space to describe your event and include anything important that we forgot to ask. -->

## Benefit of having this TMRG at GitLab

<!-- Use this space to describe your event and include anything important that we forgot to ask. -->

## Suggested mission statment of the TMRG

<!-- Use this space to describe your event and include anything important that we forgot to ask. -->

## TMRG Description

<!-- Use this space to describe your event and include anything important that we forgot to ask. -->

/label ~GitLab TMRG (Team Member Resource Group) ~"status:triage"
/assign @lmcnally1
/confidential

